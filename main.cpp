const char g_version[] = 
//
//	main.cpp
//	Defect1tfDataConverter
//
//	Created by PCCO	@2015/05/31
"1.4.2"//fix output format bug and remove some output files
;
#include "struct.h"

int main(int argc, char **argv){
	ofstream ofs;
	ifstream ifs;
	string path;
	string cellName;
	string filename;
	dataConverter dataConverterInst;

	if(argc != 6){
		cerr << "Version: " << g_version << endl << endl;
		cerr << "Usage ./defect1tfDataConverter "
			 << "[working directory(./)] [cell] [fault] [value] [config file]" << endl;
		cerr << "Structure of the working directory:" << endl;
		cerr << "./[fault]/info" << endl;
		cerr << "./[fault]/[value]" << endl;
		cerr << "./info/" << endl;
		cerr << "Files read from the working directory:" << endl;
		cerr << "./info/[cell].1tfpat (1tf pattern file)" << endl;
		cerr << "./[fault]/info/[cell].1tfdefect (defect file)" << endl;
		cerr << "./[fault]/[value]/[cell].ms* (meas file)" << endl;
		cerr << "Files write to the working directory:" << endl;
		cerr << "./[fault]/[value]/[cell].1tflvl (level data)" << endl;
		cerr << "./[fault]/[value]/[cell].1tfvar (variation data)" << endl;
		cerr << "./[fault]/[value]/[cell].1tfflt (fault data)" << endl;
		cerr << "./[fault]/[value]/[cell].2tfdefect (2tf defect file)" << endl;
		cerr << "./[fault]/[value]/[cell].1tfrpt (report)" << endl;
		cerr << "./[fault]/[value]/[cell].1tfudfm (part of UDFM)" << endl;
		exit(-1);
	}
	dataConverterInst.m_path = argv[1];
	dataConverterInst.m_path += '/';
	dataConverterInst.m_cellName = argv[2];
	dataConverterInst.m_faultName = argv[3];
	dataConverterInst.m_valueName = argv[4];
	cout << ">> Read config." << endl;
	filename = argv[5];
	openFile(ifs, filename);
	dataConverterInst.setConfig(ifs);
	ifs.close();
	cout << ">> Read 1 time frame pattern." << endl;
	filename = dataConverterInst.m_path + "info/" + dataConverterInst.m_cellName + ".1tfpat";
	openFile(ifs, filename);
	dataConverterInst.m_pattern.readSerial1tfFile(ifs);
	ifs.close();
	cout << ">> Read defect file." << endl;
	filename = dataConverterInst.m_path + 
		dataConverterInst.m_faultName + 
		"/info/" + dataConverterInst.m_cellName + 
		".1tfdefect";
	openFile(ifs, filename);
	dataConverterInst.m_fault.readSerialFile(ifs);
	ifs.close();
	cout << ">> Read ms* file." << endl;
	dataConverterInst.initializeTable();
	dataConverterInst.readAllMeasureFile();
	cout << ">> Convert data." << endl;
	dataConverterInst.transformLevelToVariation();
	dataConverterInst.transformVariationToFault();
	dataConverterInst.generateStatistics();
	dataConverterInst.generate2tfDefect();
	dataConverterInst.autoOutputFormat();
	
	cout << ">> Write 1tf json." << endl;
	filename = dataConverterInst.m_path + 
		dataConverterInst.m_faultName + "/" + dataConverterInst.m_valueName +
		"/" + dataConverterInst.m_cellName + ".1tf.json";
	openFile(ofs, filename);
	dataConverterInst.writeDataJson(ofs, dataConverterInst.m_variationTable);
	ofs.close();
	/*
	cout << ">> Write 1tf level data." << endl;
	filename = dataConverterInst.m_path + 
		dataConverterInst.m_faultName + "/" + dataConverterInst.m_valueName +
		"/" + dataConverterInst.m_cellName + ".1tflvl";
	openFile(ofs, filename);
	dataConverterInst.writeDataFile(ofs, dataConverterInst.m_levelTable);
	ofs.close();
	cout << ">> Write 1tf variation data." << endl;
	filename = dataConverterInst.m_path + 
		dataConverterInst.m_faultName + "/" + dataConverterInst.m_valueName +
		"/" + dataConverterInst.m_cellName + ".1tfvar";
	openFile(ofs, filename);
	dataConverterInst.writeDataFile(ofs, dataConverterInst.m_variationTable);
	ofs.close();
	cout << ">> Write 1tf fault data." << endl;
	filename = dataConverterInst.m_path + 
		dataConverterInst.m_faultName + "/" + dataConverterInst.m_valueName +
		"/" + dataConverterInst.m_cellName + ".1tfflt";
	openFile(ofs, filename);
	dataConverterInst.writeDataFile(ofs, dataConverterInst.m_faultTable);
	ofs.close();
	cout << ">> Write 1tf UDFM." << endl;
	filename = dataConverterInst.m_path + 
		dataConverterInst.m_faultName + "/" + dataConverterInst.m_valueName +
		"/" + dataConverterInst.m_cellName + ".1tfudfm";
	openFile(ofs, filename);
	dataConverterInst.writeUDFM(ofs);
	ofs.close();*/
	cout << ">> Write 2tf defect file." << endl;
	filename = dataConverterInst.m_path + dataConverterInst.m_faultName + 
		"/" + dataConverterInst.m_valueName + "/" + dataConverterInst.m_cellName + 
		".2tfdefect";
	openFile(ofs, filename);
	dataConverterInst.writeDefectFile(ofs);
	ofs.close();/*
	cout << ">> Write 1tf report." << endl;
	filename = dataConverterInst.m_path + 
		dataConverterInst.m_faultName + "/" + dataConverterInst.m_valueName +
		"/" + dataConverterInst.m_cellName + ".1tfrpt";
	openFile(ofs, filename);
	dataConverterInst.writeReport(ofs);
	dataConverterInst.writeReport(cout);*/
	ofs.close();
    return 0;
} 
