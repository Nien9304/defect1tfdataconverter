TARGET = ../defect1tfDataConverter
OBJECT = parser.o output.o converter.o main.o common.o config.o struct_pattern.o struct_defect.o
CC=g++
CCFLAG=-D LINUX_ENV -std=c++0x -O2
TARGET:$(OBJECT)
	$(CC) $(CCFLAG) -o $(TARGET) $(OBJECT)
#makefile knows the dependency between .o .cpp
struct.h: common.h config.h struct_pattern.h
	touch struct.h
common.o:common.cpp common.h
	$(CC) $(CCFLAG) -c common.cpp
config.o:config.cpp config.h
	$(CC) $(CCFLAG) -c config.cpp
struct_pattern.o:struct_pattern.cpp struct_pattern.h
	$(CC) $(CCFLAG) -c struct_pattern.cpp
struct_defect.o:struct_defect.cpp struct_defect.h
	$(CC) $(CCFLAG) -c struct_defect.cpp

parser.o:parser.cpp struct.h
	$(CC) $(CCFLAG) -c parser.cpp
converter.o:converter.cpp struct.h
	$(CC) $(CCFLAG) -c converter.cpp
output.o:output.cpp struct.h
	$(CC) $(CCFLAG) -c output.cpp
main.o:main.cpp struct.h
	$(CC) $(CCFLAG) -c main.cpp
clean:
	rm -f $(TARGET) $(OBJECT)