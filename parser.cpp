#include "struct.h"
void dataConverter::initializeTable(){
	//initialize
	m_levelTable.resize(m_pattern.m_outputPin.size());
	m_variationTable.resize(m_pattern.m_outputPin.size());
	m_faultTable.resize(m_pattern.m_outputPin.size());
	for(unsigned int i=0; i<m_pattern.m_outputPin.size(); ++i){
		m_levelTable[i].resize(m_fault.m_defect.size()+1);//+golden
		m_variationTable[i].resize(m_fault.m_defect.size()+1);//+golden
		m_faultTable[i].resize(m_fault.m_defect.size()+1);//+golden
		for(unsigned int j=0; j<m_fault.m_defect.size()+1; ++j){
			m_levelTable[i][j].resize(m_pattern.m_truthTable.size());
			m_variationTable[i][j].resize(m_pattern.m_truthTable.size());
			m_faultTable[i][j].resize(m_pattern.m_truthTable.size());
		}
	}
	m_currentPowerTable.resize(m_fault.m_defect.size()+1);//+golden;
	m_currentSourceTable.resize(m_fault.m_defect.size()+1);//+golden;
	for(unsigned int j=0; j<m_fault.m_defect.size()+1; ++j){
		m_currentPowerTable[j].resize(m_pattern.m_truthTable.size());//+golden;
		m_currentSourceTable[j].resize(m_pattern.m_truthTable.size());//+golden;
	}
}
void dataConverter::readAllMeasureFile(){
	ifstream ifs;
	string filename;
	for(unsigned int i=0; i<m_fault.m_defect.size()+1; ++i){//+golden
		filename = m_path + m_faultName + "/" + m_valueName + "/" + m_cellName + ".1tf.comb.ms" + int2str(i);
		openFile(ifs, filename);
		readMeasureFile(ifs, i);
		ifs.close();
	}

}
void dataConverter::readMeasureFile(ifstream &ifs, int id){
	string str, cmp;
	int index;
	getline(ifs, str);
	getline(ifs, str);
	checkId(ifs, "index");
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_inputPin.begin();
	strIt != m_pattern.m_inputPin.end(); ++strIt){
		cmp.resize(strIt->size());
		transform(strIt->begin(), strIt->end(), cmp.begin(), ::tolower);
		checkId(ifs, string("level_") + cmp);
	}
	for(UDT_STR_VEC::iterator strIt = m_pattern.m_outputPin.begin();
	strIt != m_pattern.m_outputPin.end(); ++strIt){
		cmp.resize(strIt->size());
		transform(strIt->begin(), strIt->end(), cmp.begin(), ::tolower);
		checkId(ifs, string("level_") + cmp);
	}
	checkId(ifs, "current_power");
	checkId(ifs, "current_source");
	checkId(ifs, "temper");
	checkId(ifs, "alter#");
	//read level
	for(unsigned i=0; i<m_pattern.m_truthTable.size(); ++i){
		ifs >> index;
		if(index != i+1){
			cerr << "Error: index mistach. read:" << index << " expect:" << i+1 << endl;
			exit(-1);
		}
		for(unsigned j=0; j<m_pattern.m_inputPin.size(); ++j){
			ifs >> str; //level_*
		}
		for(unsigned j=0; j<m_pattern.m_outputPin.size(); ++j){
			ifs >> str;
			if(str == "failed"){
				cerr << "Error: failed to measure output level" << endl;
				exit(-1);
			}
			else{
				m_levelTable[j][id][i] = atof(str.c_str());
			}
		}
		//current_power
		ifs >> str;
		if(str == "failed"){
			cerr << "Error: failed to measure power current" << endl;
			exit(-1);
		}
		else{
			m_currentPowerTable[id][i] = atof(str.c_str());
		}
		//current_source
		ifs >> str;
		if(str == "failed"){
			cerr << "Error: failed to measure input source current" << endl;
			exit(-1);
		}
		else{
			m_currentSourceTable[id][i] = atof(str.c_str());
		}
		ifs >> str;//temper
		checkId(ifs, int2str(id+1)); //alter#
	}
}
