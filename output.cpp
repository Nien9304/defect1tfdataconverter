#include "struct.h"

void dataConverter::autoOutputFormat(){
	m_outputDefectWidth = 3;
	for(unsigned outputIdx = 0; 
	outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
		if(m_pattern.m_outputPin[outputIdx].size() > m_outputDefectWidth){
			m_outputDefectWidth = m_pattern.m_outputPin[outputIdx].size();
		}
	}
	for(unsigned defectIdx = 0; 
	defectIdx < m_fault.m_defect.size(); ++defectIdx){
		if(m_fault.m_defect[defectIdx].m_name.size() > m_outputDefectWidth){
			m_outputDefectWidth = m_fault.m_defect[defectIdx].m_name.size();
		}
	}

	m_outputDataWidth = 3;
	if(m_pattern.m_inputPin.size() > m_outputDataWidth){
		m_outputDataWidth = m_pattern.m_inputPin.size();
	}
}/*
void dataConverter::writeDataFile(ostream &os, vector<vector<vector<int> > > &data){
	os.flags(ios::left);
	os << "#input" << SEP << m_pattern.m_inputPin.size() << endl;
	os << "#output" << SEP << m_pattern.m_outputPin.size() << endl;
	os << "#defect" << SEP << m_fault.m_defect.size() << endl;
	os << "#pattern" << SEP << m_pattern.m_truthTable.size() << endl;
	for(vector<string>::const_iterator 
		strIt = m_pattern.m_inputPin.begin();
		strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	os << endl;
	for(unsigned outputIdx = 0; 
	outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
		os.width(m_outputDefectWidth);
		os << m_pattern.m_outputPin[outputIdx] << SEP;
		for(vector<string>::const_iterator
			patternIt = m_pattern.m_truthTable.begin();
			patternIt != m_pattern.m_truthTable.end(); ++patternIt){
			os.width(m_outputDataWidth);
			os << patternIt->substr(0,m_pattern.m_inputPin.size()) << SEP;
		}
		os << endl;
		for(unsigned defectIdx = 0; 
			defectIdx < m_fault.m_defect.size(); ++defectIdx){
			os.width(m_outputDefectWidth);
			os << m_fault.m_defect[defectIdx].m_name << SEP;
			for(unsigned patternIdx = 0; 
			patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
				os.width(m_outputDataWidth);
				os << data[outputIdx][defectIdx+1][patternIdx] << SEP;
			}
			os << endl;
		}
	}
	os << "#EOF" << endl;
}*/
void dataConverter::writeDataJson(ostream &os, vector<vector<vector<double> > > &data){
	os << "{\n\t\"INPUT\":\"";
	for(vector<string>::const_iterator 
		strIt = m_pattern.m_inputPin.begin();
		strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << *strIt << ' ';
	}
	os << "\",\n\t\"OUTPUT\":\"";
	for(unsigned outputIdx = 0; 
	outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
		os << m_pattern.m_outputPin[outputIdx] << ' ';
	}
	os << "\",\n\t\"PATTERN_1TF\":\"";
	for(unsigned outputIdx = 0; 
	outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
		for(vector<string>::const_iterator
			patternIt = m_pattern.m_truthTable.begin();
			patternIt != m_pattern.m_truthTable.end(); ++patternIt){
			os.width(m_outputDataWidth);
			os << patternIt->substr(0,m_pattern.m_inputPin.size())
				<< patternIt->substr(m_pattern.m_inputPin.size()+outputIdx,1) << ' ';
		}
	}
	os << "\",\n\t\"DATA\": [" << endl;
	for(unsigned defectIdx = 0; 
		defectIdx < m_fault.m_defect.size()+1; ++defectIdx){
		if(defectIdx == 0){ 
			os << "\t\t[\"golden\",\"";
		}
		else{
			os << ",\n";
			os << "\t\t[\"" << m_fault.m_defect[defectIdx-1].m_name << "\",\"";
		}
		for(unsigned outputIdx = 0; 
		outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
			for(unsigned patternIdx = 0; 
			patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
				os.width(m_outputDataWidth);
				os << data[outputIdx][defectIdx][patternIdx] << ' ';
			}
		}
		os << "\",\"";
		for(unsigned patternIdx = 0; 
		patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
			os.width(m_outputDataWidth);
			os << m_currentPowerTable[defectIdx][patternIdx] << ' ';
		}
		os << "\",\"";
		for(unsigned patternIdx = 0; 
		patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
			os.width(m_outputDataWidth);
			os << m_currentSourceTable[defectIdx][patternIdx] << ' ';
		}
		os << "\"]";
	}
	os << "\n\t]\n}";
}
void dataConverter::writeDataFile(ostream &os, vector<vector<vector<int> > > &data){
	os.flags(ios::left);
	os << "#input" << SEP << m_pattern.m_inputPin.size() << endl;
	os << "#output" << SEP << m_pattern.m_outputPin.size() << endl;
	os << "#pattern" << SEP << m_pattern.m_truthTable.size() << endl;
	os << "#defect" << SEP << m_fault.m_defect.size() << endl;
	for(vector<string>::const_iterator 
		strIt = m_pattern.m_inputPin.begin();
		strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	os << endl;
	for(unsigned outputIdx = 0; 
	outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
		os << m_pattern.m_outputPin[outputIdx] << SEP;
	}
	os << endl;
	os << '@';
	os << SEP;
	os.width(m_outputDefectWidth);
	os << '@';
	os << SEP;
	for(unsigned outputIdx = 0; 
	outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
		for(vector<string>::const_iterator
			patternIt = m_pattern.m_truthTable.begin();
			patternIt != m_pattern.m_truthTable.end(); ++patternIt){
			os.width(m_outputDataWidth);
			os << patternIt->substr(0,m_pattern.m_inputPin.size()) << SEP;
		}
	}
	os << endl;
	for(unsigned defectIdx = 0; 
		defectIdx < m_fault.m_defect.size(); ++defectIdx){
		os << '@';
		os << SEP;
		os.width(m_outputDefectWidth);
		os << m_fault.m_defect[defectIdx].m_name << SEP;
		for(unsigned outputIdx = 0; 
		outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
			for(unsigned patternIdx = 0; 
			patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
				os.width(m_outputDataWidth);
				os << data[outputIdx][defectIdx+1][patternIdx] << SEP;
			}
		}
		os << endl;
	}
	os << "#EOF" << endl;
}
void dataConverter::writeDataFile(ostream &os, vector<vector<vector<double> > > &data){
	os.flags(ios::left);
	os << "#input" << SEP << m_pattern.m_inputPin.size() << endl;
	os << "#output" << SEP << m_pattern.m_outputPin.size() << endl;
	os << "#pattern" << SEP << m_pattern.m_truthTable.size() << endl;
	os << "#defect" << SEP << m_fault.m_defect.size() << endl;
	for(vector<string>::const_iterator 
		strIt = m_pattern.m_inputPin.begin();
		strIt != m_pattern.m_inputPin.end(); ++strIt){
		os << *strIt << SEP;
	}
	os << endl;
	for(unsigned outputIdx = 0; 
	outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
		os << m_pattern.m_outputPin[outputIdx] << SEP;
	}
	os << endl;
	os << '@';
	os << SEP;
	os.width(m_outputDefectWidth);
	os << '@';
	os << SEP;
	for(unsigned outputIdx = 0; 
	outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
		for(vector<string>::const_iterator
			patternIt = m_pattern.m_truthTable.begin();
			patternIt != m_pattern.m_truthTable.end(); ++patternIt){
			os.width(6);
			os << patternIt->substr(0,m_pattern.m_inputPin.size()) << SEP;
		}
	}
	os << endl;
	for(unsigned defectIdx = 0; 
		defectIdx < m_fault.m_defect.size(); ++defectIdx){
		os << '@';
		os << SEP;
		os.width(m_outputDefectWidth);
		os << m_fault.m_defect[defectIdx].m_name << SEP;
		for(unsigned outputIdx = 0; 
		outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
			for(unsigned patternIdx = 0; 
			patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
				os.precision(4);
				os.setf(ios::fixed);
				os << data[outputIdx][defectIdx+1][patternIdx] << SEP;
			}
		}
		os << endl;
	}
	os << "#EOF" << endl;
}
void dataConverter::writeDefectFile(ostream &os){
	os << m_cellName << SEP << m_faultName << endl;
	os << "#defect" << SEP << m_defectList.size() << endl;
	for(UDT_DEFECT_VEC::iterator defectIt = m_defectList.begin(); 
	defectIt != m_defectList.end(); ++defectIt){
		os << defectIt->m_name << SEP << defectIt->m_location.size();
		for(UDT_INT_VEC::iterator intIt = defectIt->m_location.begin();
		intIt != defectIt->m_location.end(); ++intIt){
			os << SEP << *intIt;
		}
		os << endl;
	}
	os << "#EOF";
}
void dataConverter::writeUDFM(ostream &os){
	if(m_faultCount == 0) return; //No test
	//Cell("AND2"){
	os << "\t" << "Cell(\"" << m_cellName << "\"){" << endl;	
	for(unsigned defectIdx = 0; defectIdx < m_fault.m_defect.size(); ++defectIdx){
		if(m_patternCount[defectIdx] == 0) continue;
		//Fault("ZN_0"){
		os << "\t\t" << "Fault(\"" 
			<< m_faultName << "::"
			<< m_fault.m_defect[defectIdx].m_name << "::"
			<< m_valueName
			<< "\"){" << endl;
		for(unsigned outputIdx = 0; 
		outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
			for(unsigned patternIdx = 0; 
			patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
				if(m_faultTable[outputIdx][defectIdx+1][patternIdx] == 0) continue;
				//Test{
				//	StaticFault{"Z":0; }
				//	Conditions{"A1":1; "A2":1; }
				//} //test
				os << "\t\t\t" << "Test{" << endl;
				os << "\t\t\t\t" << "StaticFault{\"" << m_pattern.m_outputPin[outputIdx] 
					<< "\":" << m_pattern.m_truthTable[patternIdx][
						m_pattern.m_inputPin.size()+outputIdx] 
					<< "; ";
				os << "}" << endl;
				os << "\t\t\t\t" << "Conditions{";
				for(unsigned inputIdx = 0; inputIdx < m_pattern.m_inputPin.size(); ++inputIdx){
					os << "\"" << m_pattern.m_inputPin[inputIdx] << "\":";
					os << m_pattern.m_truthTable[patternIdx][inputIdx] << "; ";
				}
				os << "}" << endl;
				os << "\t\t\t" << "}" << endl;
			}
		}
		//} //fault
		os << "\t\t" << "}" << endl;
	}
	//} //module
	os << "\t" << "}" << endl;
}
void dataConverter::writeReport(ostream &os){
	//Report
	os << "";
	os << m_cellName << " ";
	os << m_faultName << " ";
	os << m_valueName << endl;
	os << "Total defect: " << m_fault.m_defect.size() << endl;
	os << "Total fault: " << m_faultCount << endl;
	if(m_fault.m_defect.size() == 0){
		os << "Coverage: 100" << "%" << endl;
	}
	else{
		os << "Coverage: " << 100 * m_faultCount / m_fault.m_defect.size() << "%" << endl;
	}
}