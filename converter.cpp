#include "struct.h"

void dataConverter::transformLevelToVariation(){
	for(unsigned patternIdx = 0; 
	patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
		for(unsigned outputIdx = 0; 
		outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
			if(m_pattern.m_truthTable[patternIdx]
			[m_pattern.m_inputPin.size()+outputIdx] == '1'){
				//varition from 1
				for(unsigned defectIdx=0; 
				defectIdx <= m_fault.m_defect.size(); ++defectIdx){
					m_variationTable[outputIdx][defectIdx][patternIdx] = 
						(m_Vsupply-m_levelTable[outputIdx][defectIdx][patternIdx]);
				}
			}
			else{
				//varition from 0
				for(unsigned defectIdx = 0; 
				defectIdx <= m_fault.m_defect.size(); ++defectIdx){
					m_variationTable[outputIdx][defectIdx][patternIdx] = 
						(m_levelTable[outputIdx][defectIdx][patternIdx]);
				}
			}
		}
	}
}
void dataConverter::transformVariationToFault(){
	for(unsigned patternIdx = 0; 
	patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
		for(unsigned outputIdx = 0; 
		outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
			for(unsigned defectIdx = 0; 
			defectIdx <= m_fault.m_defect.size(); ++defectIdx){
				if(m_variationTable[outputIdx][defectIdx][patternIdx] > 
				m_levelThreshold){
					m_faultTable[outputIdx][defectIdx][patternIdx] = 1;
				}
				else{
					m_faultTable[outputIdx][defectIdx][patternIdx] = 0;
				}
			}
		}
	}
}
void dataConverter::generateStatistics(){
	m_faultCount = 0;
	m_patternCount.resize(m_fault.m_defect.size());
	for(unsigned defectIdx = 0; 
	defectIdx < m_fault.m_defect.size(); ++defectIdx){
		m_patternCount[defectIdx] = 0;
		for(unsigned outputIdx = 0; 
		outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
			for(unsigned patternIdx = 0; 
			patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
				if(m_faultTable[outputIdx][defectIdx+1][patternIdx] == 1){
					++m_patternCount[defectIdx];
				}
			}
		}
		if(m_patternCount[defectIdx] > 0){
			++m_faultCount;
		}
	}
}
void dataConverter::generate2tfDefect(){
	for(unsigned defectIdx = 0; 
	defectIdx < m_fault.m_defect.size(); ++defectIdx){
		int failedCount = 0;
		for(unsigned patternIdx = 0; 
		patternIdx < m_pattern.m_truthTable.size(); ++patternIdx){
			for(unsigned outputIdx = 0; 
			outputIdx < m_pattern.m_outputPin.size(); ++outputIdx){
				if(m_faultTable[outputIdx][defectIdx+1][patternIdx]){
					++failedCount;
				}
			}
		}
		if(failedCount == 0){
			m_defectList.push_back(m_fault.m_defect[defectIdx]);
		}
	}
}