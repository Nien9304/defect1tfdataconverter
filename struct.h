#ifndef _STRUCT_H_
#define _STRUCT_H_

#include "common.h"
#include "config.h"
#include "struct_pattern.h"
#include "struct_defect.h"

struct dataConverter{
	dataConverter():m_faultCount(0),m_outputDefectWidth(0),m_outputDataWidth(0){};
	void setConfig(istream &is){
		m_config.readConfig(is);
		m_config.getCfg(string("fault.level_threshold"), m_levelThreshold); //%
		m_config.getCfg(string("spice.voltage"), m_Vsupply);
		m_levelThreshold = m_levelThreshold*m_Vsupply/100.0; //convert from % to voltage
	}
	//input
	void initializeTable();
	void readAllMeasureFile();
	void readMeasureFile(ifstream &ifs, int id);
	//data convert
	void transformLevelToVariation();
	void transformVariationToFault();
	void generateStatistics();
	//output
	void autoOutputFormat();
	void writeDataJson(ostream &os, vector<vector<vector<double> > > &data);
	void writeDataFile(ostream &os, vector<vector<vector<int> > > &data);
	void writeDataFile(ostream &os, vector<vector<vector<double> > > &data);
	void writeUDFM(ostream &os);
	void generate2tfDefect();
	void writeDefectFile(ostream &os);
	void writeReport(ostream &os);

	//scope
	string m_path;
	string m_cellName;
	string m_faultName;
	string m_valueName;
	//shared structure
	config m_config;
	pattern m_pattern;
	fault m_fault;
	//parameter
	double m_Vsupply;
	double m_levelThreshold; //(V)
	int m_outputDefectWidth;
	int m_outputDataWidth;
	//structure
	//[output][defect][input]
	//!!! These tables include golden value with defect index 0
	vector<vector<vector<double> > > m_levelTable;
	vector<vector<vector<double> > > m_variationTable;
	vector<vector<vector<int> > > m_faultTable;
	//[defect][input]
	vector<vector<double> > m_currentPowerTable;
	vector<vector<double> > m_currentSourceTable;
	vector<int> m_patternCount;
	int m_faultCount;
	UDT_DEFECT_VEC m_defectList;

};

#endif